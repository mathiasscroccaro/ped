

TEX     = pdflatex
FLAGS   = -o
PDF     = main.pdf


main.pdf: main.tex ; $(TEX) $<  $(FLAGS) $@

all: pdf-compile clean-trash

pdf-compile: main.pdf

clean: clean-trash ; rm *.pdf

clean-trash: ; rm *.log *.aux *.out

view: main.pdf ; evince main.pdf
